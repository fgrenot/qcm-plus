-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 02 Juin 2015 à 15:01
-- Version du serveur :  5.6.24
-- Version de PHP :  5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `qcm`
--

CREATE DATABASE IF NOT EXISTS `qcm` DEFAULT CHARACTER SET = 'utf8' DEFAULT COLLATE = 'utf8_unicode_ci';
USE `qcm`;

-- --------------------------------------------------------

DELIMITER $$
--
-- Fonctions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `func_user_quiz_result`(`user_id` INT(11) UNSIGNED, `quiz_id` INT(11) UNSIGNED) RETURNS int(11)
BEGIN
	DECLARE counter INT(11);
	DECLARE `quiz_ending_date` TIMESTAMP;

	SELECT `end_date` INTO `quiz_ending_date` FROM `quiz_user` WHERE `id_user` = `user_id` AND `id_quiz` = `quiz_id`;

    IF `quiz_ending_date` = 0 THEN
		SET counter = -1;
	ELSE
		SELECT COUNT(a.id)
		INTO counter
		FROM `answer_user` AS `au`, `answer` AS `a`, `quiz_user` AS `qu`, `question` AS `q`
		WHERE au.id_user = `user_id` AND q.id_quiz = `quiz_id` AND qu.id_quiz = `quiz_id` AND q.id = a.id_question AND au.id_answer = a.id AND a.correct = 1;
	END IF;

	RETURN counter;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) unsigned NOT NULL,
  `answer` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `correct` TINYINT(1) NOT NULL,
  `id_question` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `answer_user`
--

CREATE TABLE IF NOT EXISTS `answer_user` (
  `id_user` int(11) unsigned NOT NULL,
  `id_answer` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) unsigned NOT NULL,
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `id_quiz` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `quiz_user`
--

CREATE TABLE IF NOT EXISTS `quiz_user` (
  `id_user` int(11) unsigned NOT NULL,
  `id_quiz` int(11) unsigned NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL,
  `last_name` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `admin` TINYINT(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `last_name`, `first_name`, `email`, `password`, `admin`) VALUES
(1, 'admin', 'admin', 'admin@fitec.com', 'fitec', 1),
(2, 'le', 'stagiaire', 'le.stagiaire@fitec.com', 'fitec', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`), ADD KEY `id_question` (`id_question`);

--
-- Index pour la table `answer_user`
--
ALTER TABLE `answer_user`
  ADD PRIMARY KEY (`id_user`,`id_answer`), ADD KEY `id_user` (`id_user`), ADD KEY `id_answer` (`id_answer`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`), ADD KEY `id_quiz` (`id_quiz`);

--
-- Index pour la table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `title` (`title`);

--
-- Index pour la table `quiz_user`
--
ALTER TABLE `quiz_user`
  ADD PRIMARY KEY (`id_user`,`id_quiz`), ADD KEY `id_user` (`id_user`), ADD KEY `id_quiz` (`id_quiz`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `last_name` (`last_name`,`first_name`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `answer`
--
ALTER TABLE `answer`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `answer`
--
ALTER TABLE `answer`
ADD CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`id_question`) REFERENCES `question` (`id`);

--
-- Contraintes pour la table `answer_user`
--
ALTER TABLE `answer_user`
ADD CONSTRAINT `answer_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
ADD CONSTRAINT `answer_user_ibfk_2` FOREIGN KEY (`id_answer`) REFERENCES `answer` (`id`);

--
-- Contraintes pour la table `question`
--
ALTER TABLE `question`
ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`id_quiz`) REFERENCES `quiz` (`id`);

--
-- Contraintes pour la table `quiz_user`
--
ALTER TABLE `quiz_user`
ADD CONSTRAINT `quiz_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
ADD CONSTRAINT `quiz_user_ibfk_2` FOREIGN KEY (`id_quiz`) REFERENCES `quiz` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
