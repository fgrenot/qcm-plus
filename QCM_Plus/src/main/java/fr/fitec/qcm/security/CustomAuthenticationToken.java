/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.security;

import fr.fitec.qcm.metier.User;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Florian
 */
public class CustomAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;

    Collection<GrantedAuthority> authorities;

    public CustomAuthenticationToken(User user) {
        super(user.getAuthorities());
        super.setAuthenticated(true);

        this.authorities = (Collection<GrantedAuthority>) user.getAuthorities();
        this.principal = user;
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

}
