/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.security;

import fr.fitec.qcm.metier.User;
import fr.fitec.qcm.services.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 *
 * @author Florian
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private ServiceUser serviceUser;

    public ServiceUser getServiceUser() {
        return serviceUser;
    }

    @Autowired
    public void setServiceUser(ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();

        User user = (User) serviceUser.loadUserByUsername(email);

        if (null == user) {
            throw new BadCredentialsException("User not found. The e-mail is invalid.");
        }

        if (!password.equals(user.getPassword())) {
            throw new BadCredentialsException("Wrong password.");
        }

        return new CustomAuthenticationToken(user);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.getSuperclass().equals(AbstractAuthenticationToken.class);
    }

}
