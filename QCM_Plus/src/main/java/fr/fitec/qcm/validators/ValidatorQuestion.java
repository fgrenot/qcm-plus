/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.validators;

import fr.fitec.qcm.metier.Question;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Vanghelis
 */
@Component
public class ValidatorQuestion implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(Question.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Question question = (Question) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.question.title.required");

        if (question.getTitle().length() < 3) {
            errors.rejectValue("title", "error.question.title.length");
        }
    }

}
