/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.validators;

import fr.fitec.qcm.metier.Quiz;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Vanghelis
 */
@Component
public class ValidatorQuiz implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(Quiz.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Quiz quiz = (Quiz) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.quiz.title.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "error.quiz.description.required");

        if (quiz.getTitle().length() < 3) {
            errors.rejectValue("title", "error.quiz.title.length");
        }
    }

}
