/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.validators;

import fr.fitec.qcm.metier.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Florian
 */
@Component
public class ValidatorUser implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(User.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "error.user.firstName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "error.user.lastName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "error.user.email.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.user.password.required");

        if (user.getFirstName().length() > 55) {
            errors.rejectValue("firstName", "error.user.firstName.length");
        }
        if (user.getLastName().length() > 55) {
            errors.rejectValue("lastName", "error.user.lastName.length");
        }
        if (user.getEmail().length() > 255) {
            errors.rejectValue("email", "error.user.email.length");
        }
        if (!user.getEmail().matches("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
                + "(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?")) {
            errors.rejectValue("email", "error.user.email.invalid");
        }

        removeSpace(user);
        if (user.getPassword().length() < 5 || user.getPassword().length() > 20) {
            errors.rejectValue("password", "error.user.password.length");
        }

    }

    private void removeSpace(User user) {
        String oldPassword = user.getPassword();
        String newPassword = oldPassword.replaceAll(" ", "");
        if (!oldPassword.equals(newPassword)) {
            user.setPassword(newPassword);
        }
    }

}
