/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.validators;

import fr.fitec.qcm.metier.Answer;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Vanghelis
 */
@Component
public class ValidatorAnswer implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return type.isAssignableFrom(Answer.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "answer", "error.answer.title.required");
    }

}
