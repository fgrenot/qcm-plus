/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.dao;

import fr.fitec.qcm.metier.Answer;
import fr.fitec.qcm.metier.Question;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Fitec
 */
@Repository
public class DaoAnswer extends AbstractDao<Answer> {

    @SuppressWarnings("unchecked")
    public List<Answer> selectAnswersFromQuestion(Question question) {
        List<Answer> listeAnswer = null;

        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("FROM Answer l WHERE question = :question");
            query.setEntity("question", question);

            listeAnswer = query.list();
        } catch (HibernateException ex) {
            Logger.getLogger(DaoAnswer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return listeAnswer;
    }

}
