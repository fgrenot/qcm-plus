/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.dao;

//import hibernate.HibernateUtil;
import fr.fitec.qcm.metier.Metier;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Fitec
 * @param <T>
 */
public abstract class AbstractDao<T extends Metier> implements IDao<T> {

    private SessionFactory sessionFactory;

    private Class<T> clazz;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
        return sessionFactory.openSession();
    }

    public Class<T> getMetier() {
        return clazz;
    }

    public void setMetier(Class<T> metier) {
        this.clazz = metier;
    }

    @Override
    public void insert(T metier) {
        Session session = getSession();
        session.beginTransaction();
        try {
            session.save(metier);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
            session.getTransaction().rollback();
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void update(T metier) {
        Session session = null;
        try {
            session = getSession();
            session.beginTransaction();
            session.update(metier);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void delete(T metier) {
        Session session = null;
        try {
            session = getSession();
            session.beginTransaction();
            session.delete(metier);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> selectAll() {
        List<T> metiers = null;

        Session session = null;
        try {
            session = getSession();
            Query query = session.createQuery("FROM " + clazz.getSimpleName());
            metiers = query.list();
        } catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return metiers;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T selectById(int id) {
        T metier = null;

        Session session = null;
        try {
            session = getSession();
            metier = (T) session.get(clazz, id);
        } catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return metier;
    }

}
