/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.dao;

import fr.fitec.qcm.metier.Quiz;
import fr.fitec.qcm.metier.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Florian
 */
@Repository
public class DaoUser extends AbstractDao<User> {

    public User selectByEmail(String email) {
        User user = null;

        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("FROM User WHERE email = :email");
            query.setString("email", email);

            user = (User) query.uniqueResult();
        } catch (HibernateException ex) {
            Logger.getLogger(DaoUser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return user;
    }

    public int procQuizResult(User user, Quiz quiz) {
        int result = 0;

        Session session = null;
        try {
            session = getSession();

            Query query = session.getNamedQuery("functionUserQuizResult");
            query.setParameter("user_id", user.getId());
            query.setParameter("quiz_id", quiz.getId());

            Object queryResult = query.uniqueResult();
            if (null != queryResult) {
                result = (int) queryResult;
            }
        } catch (HibernateException ex) {
            Logger.getLogger(DaoUser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return result;
    }

}
