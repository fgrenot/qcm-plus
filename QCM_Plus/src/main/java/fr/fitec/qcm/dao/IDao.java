/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.dao;

import fr.fitec.qcm.metier.Metier;
import java.util.List;

/**
 *
 * @author Florian
 * @param <T>
 */
public interface IDao<T extends Metier> {

    public void insert(T objet);

    public void update(T objet);

    public void delete(T objet);

    public List<T> selectAll();

    public T selectById(int id);

}
