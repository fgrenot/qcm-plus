/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.dao;

import fr.fitec.qcm.metier.Quiz;
import fr.fitec.qcm.metier.QuizUser;
import fr.fitec.qcm.metier.User;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Vanghelis
 */
@Repository
public class DaoQuizUser extends AbstractDao<QuizUser> {

    public QuizUser selectQuizUserfromId(Quiz quiz, User user) {
        QuizUser quizUser = null;

        Session session = null;
        try {
            session = getSession();

            Query q = session.createQuery("FROM QuizUser WHERE quiz = :quiz and user=:user");
            q.setParameter("quiz", quiz);
            q.setParameter("user", user);
            quizUser = (QuizUser) q.uniqueResult();
        } catch (HibernateException ex) {
            Logger.getLogger(DaoQuizUser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return quizUser;
    }

    @SuppressWarnings("unchecked")
    public List<QuizUser> selectAllQuizUserFromIdUser(User user) {
        List<QuizUser> listeQuizUser = null;

        Session session = null;
        try {
            session = getSession();

            Query q = session.createQuery("FROM QuizUser WHERE user=:user");
            q.setParameter("user", user);

            listeQuizUser = q.list();
        } catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return listeQuizUser;
    }

}
