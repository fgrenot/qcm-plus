/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.dao;

import fr.fitec.qcm.metier.Answer;
import fr.fitec.qcm.metier.AnswerUser;
import fr.fitec.qcm.metier.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Vanghelis
 */
@Repository
public class DaoAnswerUser extends AbstractDao<AnswerUser> {

    public AnswerUser selectAnswerUserfromId(Answer answer, User user) {
        AnswerUser answerUser = null;

        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("FROM AnswerUser WHERE answer = :answer and user=:user");
            query.setParameter("answer", answer);
            query.setParameter("user", user);

            answerUser = (AnswerUser) query.uniqueResult();
        } catch (HibernateException ex) {
            Logger.getLogger(DaoAnswerUser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return answerUser;
    }

}
