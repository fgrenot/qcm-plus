/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.configuration;

import fr.fitec.qcm.security.CustomAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.RequestContextFilter;

/**
 *
 * @author Florian
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    CustomAuthenticationProvider authentificationProvider;

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public CustomAuthenticationProvider getAuthetificationProvider() {
        return authentificationProvider;
    }

    public void setAuthetificationProvider(CustomAuthenticationProvider authetificationProvider) {
        this.authentificationProvider = authetificationProvider;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authentificationProvider).userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authenticationProvider(authentificationProvider);

        http.authorizeRequests().antMatchers("/resources/**").permitAll();
        http.authorizeRequests().antMatchers("/index*").permitAll();
        http.authorizeRequests().antMatchers("/register*").permitAll();
        http.authorizeRequests().antMatchers("/login*").permitAll();
        http.authorizeRequests().antMatchers("/j_spring_security_checks").permitAll();

        http.authorizeRequests().antMatchers("/user/**").access("hasRole('ROLE_ADMIN')");
        http.authorizeRequests().antMatchers("/**").fullyAuthenticated();

        http.formLogin()
                .loginPage("/login.htm")
                .loginProcessingUrl("/j_spring_security_check")
                .failureUrl("/login.htm")
                .usernameParameter("email")
                .passwordParameter("password");

        http.logout()
                .logoutUrl("/j_spring_security_logout")
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID");

        http.addFilterBefore(new RequestContextFilter(), UsernamePasswordAuthenticationFilter.class);
        http.csrf().disable();
    }

}
