/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.controllers;

import fr.fitec.qcm.metier.AnswerUser;
import fr.fitec.qcm.metier.AnswerUserId;
import fr.fitec.qcm.metier.Quiz;
import fr.fitec.qcm.metier.QuizUser;
import fr.fitec.qcm.metier.QuizUserId;
import fr.fitec.qcm.metier.User;
import fr.fitec.qcm.services.ServiceAnswer;
import fr.fitec.qcm.services.ServiceAnswerUser;
import fr.fitec.qcm.services.ServiceQuiz;
import fr.fitec.qcm.services.ServiceQuizUser;
import fr.fitec.qcm.services.ServiceUser;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author Fitec
 */
@Controller
@RequestMapping(value = "/quiz_user")
@SessionAttributes(value = "quizUserSession", types = fr.fitec.qcm.metier.QuizUser.class)
public class ControllerQuizUser {

    private ServiceQuizUser serviceQuizUser;
    private ServiceQuiz serviceQuiz;
    private ServiceUser serviceUser;
    private ServiceAnswerUser serviceAnswerUser;
    private ServiceAnswer serviceAnswer;

    public ServiceAnswer getServiceAnswer() {
        return serviceAnswer;
    }

    @Autowired
    public void setServiceAnswer(ServiceAnswer serviceAnswer) {
        this.serviceAnswer = serviceAnswer;
    }

    public ServiceAnswerUser getServiceAnswerUser() {
        return serviceAnswerUser;
    }

    @Autowired
    public void setServiceAnswerUser(ServiceAnswerUser serviceAnswerUser) {
        this.serviceAnswerUser = serviceAnswerUser;
    }

    public ServiceQuizUser getServiceQuizUser() {
        return serviceQuizUser;
    }

    @Autowired
    public void setServiceQuizUser(ServiceQuizUser serviceQuizUser) {
        this.serviceQuizUser = serviceQuizUser;
    }

    public ServiceQuiz getServiceQuiz() {
        return serviceQuiz;
    }

    @Autowired
    public void setServiceQuiz(ServiceQuiz serviceQuiz) {
        this.serviceQuiz = serviceQuiz;
    }

    public ServiceUser getServiceUser() {
        return serviceUser;
    }

    @Autowired
    public void setServiceUser(ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    @RequestMapping(value = "/beginQuiz", method = RequestMethod.GET)
    public String beginQuiz(@RequestParam(value = "id") int id, Model model) {
        // On récupère l'utilisateur
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        //on instancie quizUserId
        QuizUserId quizUserId = new QuizUserId();
        quizUserId.setIdQuiz(id);
        quizUserId.setIdUser(user.getId());

        //on instancie quiz
        Quiz quiz = serviceQuiz.readById(id);

        //currentDAte
        Date d = new Date();

        //on lance la requete create quizuser
        QuizUser quizUser = new QuizUser();
        quizUser.setId(quizUserId);
        quizUser.setUser(user);
        quizUser.setQuiz(quiz);
        quizUser.setStartDate(d);
        quizUser.setEndDate(d);

        serviceQuizUser.create(quizUser);

        model.addAttribute("quiz_user", quizUser);
        model.addAttribute("user", new User());
        model.addAttribute("quiz_question", quizUser.getQuiz().getQuestions());
        model.addAttribute("quizUserSession", quizUser);

        return "beginQuiz";
    }

    @RequestMapping(value = "/submitQuiz", method = RequestMethod.POST)
    public String processSubmitQuiz(User user, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userAuth = (User) auth.getPrincipal();

        user.setId(userAuth.getId());
        List<AnswerUser> userAnswers = user.getAnswerUsers();

        for (AnswerUser answer : userAnswers) {
            AnswerUserId answerUserId = new AnswerUserId();
            answerUserId.setIdAnswer(answer.getAnswer().getId());
            answerUserId.setIdUser(user.getId());
            answer.setId(answerUserId);
            serviceAnswerUser.create(answer);
        }

        Map<String, Object> maps = model.asMap();
        QuizUser quizUser = (QuizUser) maps.get("quizUserSession");
        Date debut = quizUser.getStartDate();
        //update endDate
        Date d = new Date();
        quizUser.setStartDate(debut);
        quizUser.setEndDate(d);
        serviceQuizUser.update(quizUser);

        return "redirect:/quiz/quiz.htm";
    }

}
