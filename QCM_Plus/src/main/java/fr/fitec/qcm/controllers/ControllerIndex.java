/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.controllers;

import fr.fitec.qcm.metier.User;
import fr.fitec.qcm.security.CustomAuthenticationProvider;
import fr.fitec.qcm.services.ServiceUser;
import fr.fitec.qcm.validators.ValidatorUser;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Florian
 */
@Controller
public class ControllerIndex {

    private ServiceUser serviceUser;
    private CustomAuthenticationProvider authenticationProvider;

    public ServiceUser getServiceUser() {
        return serviceUser;
    }

    @Autowired
    public void setServiceUser(ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    public CustomAuthenticationProvider getAuthenticationProvider() {
        return authenticationProvider;
    }

    @Autowired
    public void setAuthenticationProvider(CustomAuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(new ValidatorUser());
    }

    @RequestMapping(value = "/index")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@Validated User user, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "register";
        }

        serviceUser.create(user);

        doAutoLogin(user.getEmail(), user.getPassword(), request);
        return "index";
    }

    private void doAutoLogin(String username, String password, HttpServletRequest request) {
        try {
            // Must be called from request filtered by Spring Security, otherwise SecurityContextHolder is not updated
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            token.setDetails(new WebAuthenticationDetails(request));
            Authentication authentication = this.authenticationProvider.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (Exception ex) {
            Logger.getLogger(ControllerIndex.class.getName()).log(Level.SEVERE, null, ex);

            SecurityContextHolder.getContext().setAuthentication(null);
        }

    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

}
