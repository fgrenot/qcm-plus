/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.controllers;

import fr.fitec.qcm.metier.Answer;
import fr.fitec.qcm.metier.Question;
import fr.fitec.qcm.services.ServiceAnswer;
import fr.fitec.qcm.validators.ValidatorAnswer;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Vanghelis
 */
@Controller
@RequestMapping(value = "/answer")
public class ControllerAnswer {

    private ServiceAnswer serviceAnswer;
    private MessageSource messageSource;

    public ServiceAnswer getServiceAnswer() {
        return serviceAnswer;
    }

    @Autowired
    public void setServiceAnswer(ServiceAnswer serviceAnswer) {
        this.serviceAnswer = serviceAnswer;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @InitBinder
    public void initData(WebDataBinder binder) {
        binder.setValidator(new ValidatorAnswer());
    }

    @RequestMapping(value = "/createAnswer", method = RequestMethod.GET)
    public String createAnswer(Model model) {
        model.addAttribute("answer", new Answer());
        return "createAnswer";
    }

    @RequestMapping(value = "/createAnswer", method = RequestMethod.POST)
    public String createAnswer(@Validated Answer answer, Model model, BindingResult result, @RequestParam(value = "idQuestion") int idQuestion) {
        if (result.hasErrors()) {
            return "createAnswer";
        }
        Question question = new Question();
        question.setId(idQuestion);
        answer.setQuestion(question);

        serviceAnswer.create(answer);

        return "redirect:/answer/createAnswer.htm?idQuestion=" + idQuestion;
    }

    @RequestMapping(value = "/listAnswer", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String processAJAXRequest(@RequestParam("idQuestion") String idQuestion, Locale locale) {
        // Process the request
        Question question = new Question();
        question.setId(Integer.valueOf(idQuestion));
        List<Answer> answers = serviceAnswer.getAnswersFromQuestion(question);

        String modify = messageSource.getMessage("util.modify", null, locale);
        String remove = messageSource.getMessage("util.remove", null, locale);

        // Prepare the response string
        StringBuilder response = new StringBuilder();
        for (Answer answer : answers) {
            response.append("<li><b>");
            response.append(answer.getAnswer());
            response.append("</b>  = Correct ? ");
            response.append(answer.getCorrect());
            response.append("  <a href=\"/QCM_Plus/answer/modifAnswer.htm?id=");
            response.append(answer.getId());
            response.append("\">");
            response.append(modify);
            response.append("</a>  <a href=\"/QCM_Plus/answer/remove.htm?idAnswer=");
            response.append(answer.getId());
            response.append("\">");
            response.append(remove);
            response.append("</a></li>");
        }

        return response.toString();
    }

    @RequestMapping(value = "/modifAnswer", method = RequestMethod.GET)
    public String modifAnswer(@RequestParam(value = "id") int id, Model model) {
        model.addAttribute("answer", serviceAnswer.readById(id));
        return "modifAnswer";
    }

    @RequestMapping(value = "/modifAnswer", method = RequestMethod.POST)
    public String modifAnswer(@Validated Answer answer, BindingResult result) {
        if (result.hasErrors()) {
            return "modifAnswer";
        }

        serviceAnswer.update(answer);
        return "redirect:/question/modifQuestion.htm?id=" + answer.getQuestion().getId();
    }

    @RequestMapping(value = "/remove")
    public String removeAnswer(@RequestParam(value = "idAnswer") int id) {
        Answer answer = serviceAnswer.readById(id);
        int idQuestion = answer.getQuestion().getId();
        serviceAnswer.delete(answer);

        return "redirect:/answer/createAnswer.htm?idQuestion=" + idQuestion;
    }

}
