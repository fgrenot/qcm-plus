/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.controllers;

import fr.fitec.qcm.metier.Question;
import fr.fitec.qcm.metier.Quiz;
import fr.fitec.qcm.services.ServiceQuestion;
import fr.fitec.qcm.validators.ValidatorQuestion;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author Vanghelis
 */
@Controller
@RequestMapping(value = "/question")
@SessionAttributes(value = "createdQuestion", types = fr.fitec.qcm.metier.Quiz.class)
public class ControllerQuestion {

    private ServiceQuestion serviceQuestion;
    private MessageSource messageSource;

    public ServiceQuestion getServiceQuestion() {
        return serviceQuestion;
    }

    @Autowired
    public void setServiceQuestion(ServiceQuestion serviceQuestion) {
        this.serviceQuestion = serviceQuestion;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @InitBinder
    public void initData(WebDataBinder binder) {
        binder.setValidator(new ValidatorQuestion());
    }

    @RequestMapping(value = "/createQuestion", method = RequestMethod.GET)
    public String createQuestion(Model model) {
        model.addAttribute("question", new Question());
        return "createQuestion";
    }

    @RequestMapping(value = "/createQuestion", method = RequestMethod.POST)
    public String createQuestion(@Validated Question question, Model model, BindingResult result, @RequestParam(value = "idQuiz") int idQuiz) {
        if (result.hasErrors()) {
            return "createQuestion";
        }

        Quiz quiz = new Quiz();
        quiz.setId(idQuiz);
        question.setQuiz(quiz);

        serviceQuestion.create(question);

        return "redirect:/answer/createAnswer.htm?idQuestion=" + question.getId();
    }

    @RequestMapping(value = "/listQuestion", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String processAJAXRequest(@RequestParam("idQuiz") String idQuiz, Locale locale) {
        // Process the request
        Quiz quiz = new Quiz();
        quiz.setId(Integer.valueOf(idQuiz));
        List<Question> questions = serviceQuestion.getQuestionsFromQuiz(quiz);

        String modify = messageSource.getMessage("util.modify", null, locale);
        String remove = messageSource.getMessage("util.remove", null, locale);
        String answers = messageSource.getMessage("createAnswer.answers", null, locale);

        // Prepare the response string
        StringBuilder response = new StringBuilder();
        for (Question question : questions) {
            response.append("<li><b>");
            response.append(question.getTitle());
            response.append("</b>  <a href=\"/QCM_Plus/question/modifQuestion.htm?id=");
            response.append(question.getId());
            response.append(" \">");
            response.append(modify);
            response.append("</a>  <a href=\"/QCM_Plus/question/remove.htm?id=");
            response.append(question.getId());
            response.append("\">");
            response.append(remove);
            response.append("</a>         <a href=\"/QCM_Plus/answer/createAnswer.htm?idQuestion=");
            response.append(question.getId());
            response.append(" \">");
            response.append(answers);
            response.append("</a></li>");
        }

        return response.toString();
    }

    @RequestMapping(value = "/modifQuestion", method = RequestMethod.GET)
    public String modifQuestion(@RequestParam(value = "id") int id, Model model) {
        model.addAttribute("question", serviceQuestion.readById(id));
        return "modifQuestion";
    }

    @RequestMapping(value = "/modifQuestion", method = RequestMethod.POST)
    public String modifQuestion(@Validated Question question, BindingResult result) {
        if (result.hasErrors()) {
            return "modifQuestion";
        }

        serviceQuestion.update(question);
        return "redirect:/index.htm";
    }

    @RequestMapping(value = "/remove")
    public String removeQuestion(@RequestParam(value = "id") int id) {
        Question question = serviceQuestion.readById(id);
        serviceQuestion.delete(question);
        return "redirect:/index.htm";
    }

}
