/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.controllers;

import fr.fitec.qcm.metier.User;
import fr.fitec.qcm.services.ServiceUser;
import fr.fitec.qcm.validators.ValidatorUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Florian
 */
@Controller
@RequestMapping(value = "/user")
public class ControllerUser {

    private ServiceUser serviceUser;

    public ServiceUser getServiceUser() {
        return serviceUser;
    }

    @Autowired
    public void setServiceUser(ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(new ValidatorUser());
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("user", new User());
        return "addUser";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            return "addUser";
        }

        serviceUser.create(user);

        return "redirect:/user/view.htm";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("users", serviceUser.readAll());
        return "viewUsers";
    }

    @RequestMapping(value = "/modify", method = RequestMethod.GET)
    public String modify(@RequestParam(value = "id") int id, Model model) {
        model.addAttribute("user", serviceUser.readById(id));
        return "modifyUser";
    }

    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public String modify(@Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            return "modifyUser";
        }

        serviceUser.update(user);

        return "redirect:/user/view.htm";
    }

    @RequestMapping(value = "/remove")
    public String remove(@RequestParam(value = "id") int id) {
        User user = serviceUser.readById(id);
        serviceUser.delete(user);

        return "redirect:/user/view.htm";
    }

}
