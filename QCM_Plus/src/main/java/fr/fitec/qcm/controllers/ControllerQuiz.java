/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.controllers;

import fr.fitec.qcm.metier.Quiz;
import fr.fitec.qcm.metier.QuizUser;
import fr.fitec.qcm.metier.User;
import fr.fitec.qcm.services.ServiceQuiz;
import fr.fitec.qcm.services.ServiceQuizUser;
import fr.fitec.qcm.services.ServiceUser;
import fr.fitec.qcm.validators.ValidatorQuiz;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Vanghelis
 */
@Controller
@RequestMapping(value = "/quiz")
public class ControllerQuiz {

    private ServiceQuiz serviceQuiz;
    private ServiceQuizUser serviceQuizUser;
    private ServiceUser serviceUser;

    public ServiceQuiz getServiceQuiz() {
        return serviceQuiz;
    }

    @Autowired
    public void setServiceQuiz(ServiceQuiz serviceQuiz) {
        this.serviceQuiz = serviceQuiz;
    }

    public ServiceQuizUser getServiceQuizUser() {
        return serviceQuizUser;
    }

    @Autowired
    public void setServiceQuizUser(ServiceQuizUser serviceQuizUser) {
        this.serviceQuizUser = serviceQuizUser;
    }

    public ServiceUser getServiceUser() {
        return serviceUser;
    }

    @Autowired
    public void setServiceUser(ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    @InitBinder
    public void initData(WebDataBinder binder) {
        binder.setValidator(new ValidatorQuiz());
    }

    @RequestMapping(value = "/createQuiz", method = RequestMethod.GET)
    public String createQuiz(Model model) {
        model.addAttribute("quiz", new Quiz());
        return "createQuiz";
    }

    @RequestMapping(value = "/createQuiz", method = RequestMethod.POST)
    public String createQuiz(@Validated Quiz quiz, Model model, BindingResult result) {
        if (result.hasErrors()) {
            return "createQuiz";
        }

        serviceQuiz.create(quiz);

        model.addAttribute("createdQuiz", quiz);
        return "redirect:/question/createQuestion.htm?idQuiz=" + quiz.getId();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String processAJAXRequest() {
        // Process the request
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User principal = (User) auth.getPrincipal();

        List<Quiz> quizs = serviceQuiz.readAll();

        // Prepare the response string
        StringBuilder response = new StringBuilder();
        for (Quiz quiz : quizs) {
            if (principal.isAdmin()) {
                response.append("<li><a href=\"/QCM_Plus/quiz/modifQuiz.htm?id=");
            } else {
                response.append("<li><a href=\"/QCM_Plus/quiz_user/beginQuiz.htm?id=");
            }
            response.append(quiz.getId());
            response.append("\">");
            response.append(quiz.getTitle());
            response.append("</a></li>\n");
        }

        return response.toString();
    }

    @RequestMapping(value = "/listQuiz")
    public String listQuiz(Model model) {
        model.addAttribute("lesQuiz", serviceQuiz.readAll());
        return "listQuiz";
    }

    @RequestMapping(value = "/quiz")
    public String userListQuiz(Model model) {
        // On récupère l'utilisateur
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        List<QuizUser> quizUsers = serviceQuizUser.getAllQuizUser(user);
        model.addAttribute("quiz_user", quizUsers);

        Map<Integer, Integer> quizUserResults = new HashMap<>(quizUsers.size());
        for (QuizUser quizUser : quizUsers) {
            Quiz currentQuiz = quizUser.getQuiz();
            quizUserResults.put(currentQuiz.getId(), serviceUser.quizResult(user, currentQuiz));
        }
        model.addAttribute("quiz_results", quizUserResults);

        Map<Integer, Integer> quizUserMax = new HashMap<>(quizUsers.size());
        for (QuizUser quizUser : quizUsers) {
            Quiz currentQuiz = quizUser.getQuiz();
            quizUserMax.put(currentQuiz.getId(), currentQuiz.getQuestions().size());
        }
        model.addAttribute("quiz_max", quizUserMax);

        List<Quiz> newQuiz = serviceQuiz.readAll();

        for (QuizUser quizUser : quizUsers) {
            newQuiz.remove(quizUser.getQuiz());
        }

        model.addAttribute("quiz", newQuiz);

        return "quiz";
    }

    @RequestMapping(value = "/modifQuiz", method = RequestMethod.GET)
    public String modifQuiz(@RequestParam(value = "id") int id, Model model) {
        model.addAttribute("quiz", serviceQuiz.readById(id));
        return "modifQuiz";
    }

    @RequestMapping(value = "/modifQuiz", method = RequestMethod.POST)
    public String modifQuiz(@Validated Quiz quiz, BindingResult result) {
        if (result.hasErrors()) {
            return "modifQuiz";
        }

        serviceQuiz.update(quiz);
        return "redirect:/quiz/listQuiz.htm";
    }

    @RequestMapping(value = "/remove")
    public String removeQuiz(@RequestParam(value = "id") int id) {
        Quiz quiz = serviceQuiz.readById(id);
        serviceQuiz.delete(quiz);
        return "redirect:/quiz/listQuiz.htm";
    }

}
