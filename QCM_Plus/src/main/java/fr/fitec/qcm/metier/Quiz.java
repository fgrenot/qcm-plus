package fr.fitec.qcm.metier;
// Generated 22 juin 2015 13:50:18 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Quiz generated by hbm2java
 */
@Entity
@Table(name = "quiz", catalog = "qcm")
public class Quiz extends Metier implements java.io.Serializable {

    private Integer id;
    private String title;
    private String description;
    private Set<QuizUser> quizUsers = new HashSet<>(0);
    private Set<Question> questions = new HashSet<>(0);

    public Quiz() {
    }

    public Quiz(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Quiz(String title, String description, Set<QuizUser> quizUsers, Set<Question> questions) {
        this.title = title;
        this.description = description;
        this.quizUsers = quizUsers;
        this.questions = questions;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "title", nullable = false)
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "description", nullable = false)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "quiz")
    public Set<QuizUser> getQuizUsers() {
        return this.quizUsers;
    }

    public void setQuizUsers(Set<QuizUser> quizUsers) {
        this.quizUsers = quizUsers;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "quiz", cascade = CascadeType.REMOVE)
    public Set<Question> getQuestions() {
        return this.questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Quiz other = (Quiz) obj;
        return Objects.equals(this.id, other.id);
    }

}
