package fr.fitec.qcm.metier;
// Generated 22 juin 2015 13:50:18 by Hibernate Tools 4.3.1

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Vanghelis
 */
@Entity
@Table(name = "answer_user", catalog = "qcm")
public class AnswerUser extends Metier implements Serializable {

    private AnswerUserId id;
    private Answer answer;
    private User user;

    public AnswerUser() {
    }

    public AnswerUser(AnswerUserId id, Answer answer, User user) {
        this.id = id;
        this.answer = answer;
        this.user = user;
    }

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "idUser", column = @Column(name = "id_user", nullable = false)),
        @AttributeOverride(name = "idAnswer", column = @Column(name = "id_answer", nullable = false))})
    public AnswerUserId getId() {
        return this.id;
    }

    public void setId(AnswerUserId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_answer", nullable = false, insertable = false, updatable = false)
    public Answer getAnswer() {
        return this.answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", nullable = false, insertable = false, updatable = false)
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnswerUser other = (AnswerUser) obj;
        return Objects.equals(this.id, other.id);
    }

}
