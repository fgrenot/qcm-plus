/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.metier.Metier;
import java.util.List;

/**
 *
 * @author Florian
 * @param <T>
 */
public interface IService<T extends Metier> {

    public void create(T objet);

    public void update(T objet);

    public void delete(T objet);

    public List<T> readAll();

    public T readById(int id);

}
