/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.dao.AbstractDao;
import fr.fitec.qcm.dao.DaoUser;
import fr.fitec.qcm.metier.Quiz;
import fr.fitec.qcm.metier.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Florian
 */
@Service
public class ServiceUser extends AbsrtactService<User> implements UserDetailsService {

    private AbstractDao<User> dao;

    @Override
    public AbstractDao<User> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<User> dao) {
        this.dao = dao;
        dao.setMetier(User.class);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = ((DaoUser) dao).selectByEmail(username);

        if (null == user) {
            throw new UsernameNotFoundException("The user: " + username + " doesn't exist.");
        }

        return user;
    }

    public int quizResult(User user, Quiz quiz) {
        return ((DaoUser) dao).procQuizResult(user, quiz);
    }

}
