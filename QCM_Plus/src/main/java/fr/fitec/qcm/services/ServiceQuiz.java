/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.dao.AbstractDao;
import fr.fitec.qcm.metier.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vanghelis
 */
@Service
public class ServiceQuiz extends AbsrtactService<Quiz> {

    private AbstractDao<Quiz> dao;

    @Override
    public AbstractDao<Quiz> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<Quiz> dao) {
        this.dao = dao;
        dao.setMetier(Quiz.class);
    }

}
