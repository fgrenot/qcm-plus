/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.dao.AbstractDao;
import fr.fitec.qcm.dao.DaoAnswerUser;
import fr.fitec.qcm.metier.Answer;
import fr.fitec.qcm.metier.AnswerUser;
import fr.fitec.qcm.metier.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vanghelis
 */
@Service
public class ServiceAnswerUser extends AbsrtactService<AnswerUser> {

    private AbstractDao<AnswerUser> dao;

    @Override
    public AbstractDao<AnswerUser> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<AnswerUser> dao) {
        this.dao = dao;
        dao.setMetier(AnswerUser.class);
    }

    public AnswerUser getAnswerUser(Answer answer, User user) {
        return ((DaoAnswerUser) dao).selectAnswerUserfromId(answer, user);
    }

}
