/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.dao.AbstractDao;
import fr.fitec.qcm.dao.DaoQuizUser;
import fr.fitec.qcm.metier.Quiz;
import fr.fitec.qcm.metier.QuizUser;
import fr.fitec.qcm.metier.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vanghelis
 */
@Service
public class ServiceQuizUser extends AbsrtactService<QuizUser> {

    private AbstractDao<QuizUser> dao;

    @Override
    public AbstractDao<QuizUser> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<QuizUser> dao) {
        this.dao = dao;
        dao.setMetier(QuizUser.class);
    }

    public QuizUser getQuizUser(Quiz quiz, User user) {
        return ((DaoQuizUser) dao).selectQuizUserfromId(quiz, user);
    }

    public List<QuizUser> getAllQuizUser(User user) {
        return ((DaoQuizUser) dao).selectAllQuizUserFromIdUser(user);
    }

}
