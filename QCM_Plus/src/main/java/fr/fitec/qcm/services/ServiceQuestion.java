/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.dao.AbstractDao;
import fr.fitec.qcm.dao.DaoQuestion;
import fr.fitec.qcm.metier.Question;
import fr.fitec.qcm.metier.Quiz;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vanghelis
 */
@Service
public class ServiceQuestion extends AbsrtactService<Question> {

    private AbstractDao<Question> dao;

    @Override
    public AbstractDao<Question> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<Question> dao) {
        this.dao = dao;
        dao.setMetier(Question.class);
    }

    public List<Question> getQuestionsFromQuiz(Quiz quiz) {
        return ((DaoQuestion) dao).selectQuestionsFromQuiz(quiz);
    }

}
