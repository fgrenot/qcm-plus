/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.dao.AbstractDao;
import fr.fitec.qcm.metier.Metier;
import java.util.List;

/**
 *
 * @author Florian
 * @param <T>
 */
public abstract class AbsrtactService<T extends Metier> implements IService<T> {

    public abstract AbstractDao<T> getDao();

    public abstract void setDao(AbstractDao<T> dao);

    @Override
    public T readById(int id) {
        return getDao().selectById(id);
    }

    @Override
    public List<T> readAll() {
        return getDao().selectAll();
    }

    @Override
    public void delete(T objet) {
        getDao().delete(objet);
    }

    @Override
    public void update(T objet) {
        getDao().update(objet);
    }

    @Override
    public void create(T objet) {
        getDao().insert(objet);
    }

}
