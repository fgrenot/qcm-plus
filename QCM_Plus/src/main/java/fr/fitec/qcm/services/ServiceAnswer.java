/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fitec.qcm.services;

import fr.fitec.qcm.dao.AbstractDao;
import fr.fitec.qcm.dao.DaoAnswer;
import fr.fitec.qcm.metier.Answer;
import fr.fitec.qcm.metier.Question;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Vanghelis
 */
@Service
public class ServiceAnswer extends AbsrtactService<Answer> {

    private AbstractDao<Answer> dao;

    @Override
    public AbstractDao<Answer> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<Answer> dao) {
        this.dao = dao;
        dao.setMetier(Answer.class);
    }

    public List<Answer> getAnswersFromQuestion(Question question) {
        return ((DaoAnswer) dao).selectAnswersFromQuestion(question);
    }

}
