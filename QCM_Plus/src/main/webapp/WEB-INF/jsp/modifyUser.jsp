<%--
    Document   : modifUser
    Created on : 15 juin 2015, 11:28:32
    Author     : Florian
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@include file="include/header.jspf" %>

<h1 class="col-md-offset-1"><i18n:message code="modifyUser.title" text="missing" /></h1>
<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-4">
        <spr:form method="post" action="${pageContext.request.contextPath}/user/modify.htm" modelAttribute="user">
            <spr:hidden path="id"/>
            <spr:label path="lastName"><i18n:message code="user.lastname" text="missing" /> : </spr:label>
            <spr:errors path="lastName" cssClass="alert-danger"/>
            <spr:input path="lastName" cssClass="form-control"/>
            <spr:label path="firstName"><i18n:message code="user.firstname" text="missing" /> : </spr:label>
            <spr:errors path="firstName" cssClass="alert-danger"/>
            <spr:input path="firstName" cssClass="form-control"/>
            <spr:label path="email"><i18n:message code="user.email" text="missing" /> : </spr:label>
            <spr:errors path="email" cssClass="alert-danger"/>
            <spr:input path="email" cssClass="form-control"/>
            <spr:label path="password"><i18n:message code="user.password" text="missing" /> : </spr:label>
            <spr:errors path="password" cssClass="alert-danger"/>
            <spr:password path="password" cssClass="form-control"/>
            <spr:label path="admin"><i18n:message code="user.admin" text="missing" /> :</spr:label>
            <spr:checkbox path="admin" cssClass="checkbox-inline"/><br/>

            <input type="submit" value="<i18n:message code="util.modify" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>
</div>

<%@include file="include/footer.jspf" %>