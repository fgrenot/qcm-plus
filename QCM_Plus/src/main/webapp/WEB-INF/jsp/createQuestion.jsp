<%--
    Document   : createQuestion
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<h2><i18n:message code="createQuestion.title" text="missing" /></h2>
<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-4">
        <spr:form method="post" modelAttribute="question">
            <spr:hidden path="id"/>
            <spr:label path="title"><i18n:message code="question.title" text="missing" /> : </spr:label>
            <spr:errors path="title" cssClass="alert-danger"/>
            <spr:input path="title" cssClass="form-control"/>
            <input type="submit" value="<i18n:message code="createQuestion.create" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>
</div>
<h3><i18n:message code="createQuestion.questions" text="missing" /></h3>
<div class="row">
    <ul class="listQuestions">

    </ul>
</div>

<script>
    $(document).ready(function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/question/listQuestion.htm?idQuiz=" + $.urlParam('idQuiz'),
            type: "GET",
            success: function (_html) {
                $('.listQuestions').html(_html);
                //alert('sdsss');
            }
        });
    });

    //get url parameters
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results === null) {
            return null;
        } else {
            return results[1] || 0;
        }
    };

</script>

<%@include file="include/footer.jspf" %>