<%@ page contentType="text/html; charset=UTF-8" %>

<%@include file="include/header.jspf" %>

<h1><i18n:message code="error.title" text="missing" /></h1>

<p><i18n:message code="error.description" text="missing" /></p>

<%@include file="include/footer.jspf" %>