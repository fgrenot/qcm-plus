<%--
    Document   : createQuiz
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@include file="include/header.jspf" %>

<h1 class="col-md-offset-1"><i18n:message code="createQuiz.title" text="missing" /></h1>
<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-4">
        <spr:form method="post"  modelAttribute="quiz">
            <spr:hidden path="id"/>
            <spr:label path="title"><i18n:message code="quiz.title" text="missing" /> : </spr:label>
            <spr:errors path="title" cssClass="alert-danger"/>
            <spr:input path="title" cssClass="form-control"/>
            <spr:label path="description"><i18n:message code="quiz.description" text="missing" /> : </spr:label>
            <spr:errors path="description" cssClass="alert-danger"/>
            <spr:input path="description" cssClass="form-control"/>

            <input type="submit" value="<i18n:message code="createQuiz.create" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>
</div>

<%@include file="include/footer.jspf" %>