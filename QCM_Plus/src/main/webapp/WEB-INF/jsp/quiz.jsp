<%--
    Document   : listQuiz
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<h2><i18n:message code="quizPage.title" text="missing" /> ${user.lastName} ${user.firstName}</h2>
<div class="row">
    <h3><i18n:message code="quizPage.endedQuiz" text="missing" /></h3>
    <c:if test="${quiz_user.size() eq 0}">
        <i18n:message code="quizPage.noEndedQuiz" text="missing" />
    </c:if>
    <c:set var="quizUserResults" value="${requestScope['quiz_results']}"/>
    <c:set var="quizUserMax" value="${requestScope['quiz_max']}"/>
    <c:forEach var="old_quiz" items="${quiz_user}">
        <h4>${old_quiz.quiz.title}</h4>
        <p><i18n:message code="quizPage.quizResult" text="missing"
                      arguments="${old_quiz.startDate},${old_quiz.endDate},${quizUserResults[(old_quiz.quiz.id).intValue()]},${quizUserMax[(old_quiz.quiz.id).intValue()]}"
                      htmlEscape="false" /></p>
        </c:forEach>

    <h3><i18n:message code="quizPage.newQuiz" text="missing" /></h3>
    <c:if test="${quiz.size() eq 0}">
        <i18n:message code="quizPage.noNewQuiz" text="missing" />
    </c:if>
    <c:forEach var="quiz" items="${quiz}">
        <p>${quiz.title}
            <a href="<%=request.getContextPath()%>/quiz_user/beginQuiz.htm?id=${quiz.id}"><i18n:message code="util.launch" text="missing" /></a>
        </p>
    </c:forEach>
</div>

<%@include file="include/footer.jspf" %>