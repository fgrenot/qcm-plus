<%--
    Document   : listQuiz
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@include file="include/header.jspf" %>

<h1 class="col-md-offset-1"><i18n:message code="listQuiz.title" text="missing" /></h1>

<div class="row">
    <c:if test="${lesQuiz.size() == 0}">
        <h3><i18n:message code="listQuiz.noQuiz" text="missing" /></h3>
        <a href="${pageContext.request.contextPath}/quiz/createQuiz.htm"><i class="fa fa-plus"></i> <i18n:message code="navigation.createQuiz" text="missing" /> !</a>
    </c:if>
    <c:forEach var="quiz" items="${lesQuiz}">
        <p>${quiz.title}
            <a href="<%=request.getContextPath()%>/quiz/modifQuiz.htm?id=${quiz.id}"><i18n:message code="util.modify" text="missing" /></a>
            <a href="<%=request.getContextPath()%>/quiz/remove.htm?id=${quiz.id}"><i18n:message code="util.remove" text="missing" /></a>
            <a href="<%=request.getContextPath()%>/question/createQuestion.htm?idQuiz=${quiz.id}"><i18n:message code="listQuiz.manageQuestion" text="missing" /></a>
        </p>
    </c:forEach>
</div>
<%@include file="include/footer.jspf" %>