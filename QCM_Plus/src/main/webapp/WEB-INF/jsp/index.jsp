<%@ page contentType="text/html; charset=UTF-8" %>

<%@include file="include/header.jspf" %>

<h1><i18n:message code="index.welcome" text="missing" /></h1>

<p><i18n:message code="index.siteDescription" text="missing" /></p>

<p><i18n:message code="index.developers" text="missing" /></p>

<%@include file="include/footer.jspf" %>