<%--
    Document   : createAnswer
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<h2><i18n:message code="createAnswer.title" text="missing" /></h2>
<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-4">
        <spr:form method="post" modelAttribute="answer">
            <spr:hidden path="id"/>
            <spr:hidden path="question.id"/>
            <spr:label path="answer"><i18n:message code="answer.title" text="missing" /> : </spr:label>
            <spr:errors path="answer" cssClass="alert-danger"/>
            <spr:input path="answer" cssClass="form-control"/>
            <spr:label path="correct"><i18n:message code="answer.correct" text="missing" /> : </spr:label>
            <spr:checkbox path="correct"  cssClass="checkbox-inline"/><br/>
            <input type="submit" value="<i18n:message code="createAnswer.create" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>

</div>
<h3><i18n:message code="createAnswer.answers" text="missing" /></h3>
<div class="row">
    <ul class="listAnswers"></ul>
</div>

<script>
    $(document).ready(function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/answer/listAnswer.htm?idQuestion=" + $.urlParam('idQuestion'),
            type: "GET",
            success: function (_html) {
                $('.listAnswers').html(_html);
                //alert('sdsss');
            }
        });
    });

    //get url parameters
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results === null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    };
</script>

<%@include file="include/footer.jspf" %>