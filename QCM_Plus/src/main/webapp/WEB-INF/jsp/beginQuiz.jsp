<%--
    Document   : beginQuiz
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<h1>${quiz.title}</h1>

<div class="row">
    <div>
        <spr:form method="post" action="${pageContext.request.contextPath}/quiz_user/submitQuiz.htm" modelAttribute="user" id="submitQuiz">
            <c:forEach var="question" items="${quiz_question}" varStatus="loopStatus">
                <p><i>${question.title}</i></p>
                <c:forEach var="answera" items="${question.answers}" varStatus="loopStatus2">
                    <div class="radio col-md-offset-1">
                        <spr:radiobutton label="${answera.answer}" path="answerUsers[${loopStatus.index}].answer.id" value="${answera.id}"  />
                    </div>
                </c:forEach>
            </c:forEach>
            <br/>

            <input type="submit" value="<i18n:message code="util.submit" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>
</div>

<%@include file="include/footer.jspf" %>