<%--
    Document   : modifyAnswer
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<h1 class="col-md-offset-1"><i18n:message code="modifAnswer.title" text="missing" /></h1>
<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-4">
        <spr:form method="post" action="${pageContext.request.contextPath}/answer/modifAnswer.htm" modelAttribute="answer" accept-charset="UTF-8">
            <spr:hidden path="id"/>
            <spr:hidden path="question.id"/>
            <spr:label path="answer"><i18n:message code="answer.title" text="missing" /> : </spr:label>
            <spr:errors path="answer" cssClass="alert-danger"/>
            <spr:input path="answer" cssClass="form-control"/>
            <spr:label path="correct"><i18n:message code="answer.correct" text="missing" /> :</spr:label>
            <spr:checkbox path="correct" cssClass="checkbox-inline"/><br/>

            <input type="submit" value="<i18n:message code="util.modify" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>
</div>
<%@include file="include/footer.jspf" %>