<%--
    Document   : modifQuestion
    Created on : 15 juin 2015, 11:28:32
    Author     : Vanghelis
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<h1 class="col-md-offset-1"><i18n:message code="modifQuestion.title" text="missing" /></h1>
<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-4">
        <spr:form method="post" action="${pageContext.request.contextPath}/question/modifQuestion.htm" modelAttribute="question">
            <spr:hidden path="id"/>
            <spr:hidden path="quiz.id"/>
            <spr:label path="title"><i18n:message code="question.title" text="missing" /> : </spr:label>
            <spr:errors path="title" cssClass="alert-danger"/>
            <spr:input path="title" cssClass="form-control"/>
            <input type="submit" value="<i18n:message code="util.modify" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>

</div>

<h3><i18n:message code="createAnswer.answers" text="missing" /></h3>
<div class="row">
    <ul class="listAnswer">

    </ul>
</div>

<script>
    $(document).ready(function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/answer/listAnswer.htm?idQuestion=" + $.urlParam('id'),
            type: "GET",
            success: function (_html) {
                $('.listAnswer').html(_html);
            }
        });
    });

//get url parameters
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    }

</script>
<%@include file="include/footer.jspf" %>