<%--
    Document   : login
    Created on : 15 juin 2015, 11:41:06
    Author     : Florian
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@include file="include/header.jspf" %>

<h1 class="col-md-offset-1"><i18n:message code="user.login" text="missing" /></h1>
<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-4">
        <spr:form method="post" action="j_spring_security_check" modelAttribute="user" name="loginForm">
            <spr:label path="email"><i18n:message code="user.email" text="missing" /> :</spr:label>
            <spr:input path="email" title="E-mail" cssClass="form-control"/>
            <spr:label path="password"><i18n:message code="user.password" text="missing" /> :</spr:label>
            <spr:password path="password" title="Password" cssClass="form-control"/>

            <input type="submit" value="<i18n:message code="user.login" text="missing" />" class="btn btn-default"/>
        </spr:form>
    </div>
</div>

<%@include file="include/footer.jspf" %>