<%--
    Document   : viewUsers
    Created on : 22 mai 2015, 11:41:34
    Author     : Florian
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<h1><i18n:message code="viewUsers.title" text="missing" /></h1>
<div class="row clearfix">
    <table class="table table-bordered table-striped" id="tab_logic">
        <thead>
            <tr class="table-hover">
                <th title="lastname" class="text-center"><i18n:message code="user.lastname" text="missing" /></th>
                <th title="firstname" class="text-center"><i18n:message code="user.firstname" text="missing" /></th>
                <th title="email" class="text-center"><i18n:message code="user.email" text="missing" /></th>
                <th title="password" class="text-center"><i18n:message code="user.password" text="missing" /></th>
                <th title="admin" class="text-center"><i18n:message code="user.admin" text="missing" /></th>
                <th title="action" class="text-center"><i18n:message code="util.action" text="missing" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${users}" var="user" >
                <tr class="tab-content">
                    <td headers="lastname">${user.lastName}</td>
                    <td headers="prenom">${user.firstName}</td>
                    <td headers="email">${user.email}</td>
                    <td headers="password">${user.password}</td>
                    <td headers="admin">${user.admin}</td>
                    <td headers="action">
                        <a href="${pageContext.request.contextPath}/user/modify.htm?id=${user.id}"><i class="fa fa-edit"></i></a>
                        <a href="${pageContext.request.contextPath}/user/remove.htm?id=${user.id}"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<a href="${pageContext.request.contextPath}/user/add.htm" class="btn btn-default pull-left"><i18n:message code="navigation.addUser" text="missing" /></a>

<%@include file="include/footer.jspf" %>